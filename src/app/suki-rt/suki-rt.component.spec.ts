import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SukiRTComponent } from './suki-rt.component';

describe('SukiRTComponent', () => {
  let component: SukiRTComponent;
  let fixture: ComponentFixture<SukiRTComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SukiRTComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SukiRTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
