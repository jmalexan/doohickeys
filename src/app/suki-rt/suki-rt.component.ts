import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suki-rt',
  templateUrl: './suki-rt.component.html',
  styleUrls: ['./suki-rt.component.scss']
})
export class SukiRTComponent implements OnInit {

  public file?: File = undefined;
  public delay?: number = undefined;

  public step1: boolean = false;
  public step2: boolean = false;
  public step3: boolean = false;

  public step1err: boolean = false;
  public step2err: boolean = false;
  public step3err: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  newFile(event: Event): void {
    const input: HTMLInputElement = event.target as HTMLInputElement;

    if (!input.files?.length) {
      return;
    }

    this.file = input.files[0];
    this.step1 = true;
    this.step1err = false;
  }

  newDelay(event: Event): void {
    const input: HTMLInputElement = event.target as HTMLInputElement;

    const delayValidation: RegExp = /^-?[0-9]*(\.[0-9]*)?$/g;

    let delaytext: string = input.value.trim();

    let isNumber: boolean = delayValidation.test(delaytext);

    if (isNumber) {
      this.delay = parseFloat(delaytext);
      this.step2 = true;
      this.step2err = false;
    } else {
      this.delay = undefined;
      this.step2 = false;
    }
  }

  processFile(): void {
    this.step3err = false;

    if (this.file == undefined) {
      this.step1err = true;
    }

    if (this.delay == undefined) {
      this.step2err = true;
    }

    if (this.step1err || this.step2err) {
      return;
    }

    this.step3 = true;

    this.file?.text().then(text => {
      if (this.delay == undefined || this.file == undefined) {
        throw 'Somehow the delay or file got unset as the file was being read';
      }
      let lines: string[] = text.split(/\r?\n/);
      let newlines: string[] = [];
      let subid = 1;
      for (let i = 0; i < lines.length; i++) {
        if (lines[i] == subid.toString()) {
          newlines.push(lines[i]);
          i++;
          subid++;
          let timestamps = lines[i].split(/ --> /);
          let newtimestamps: string[] = [];
          for (let timestamp of timestamps) {
            let time = timestamp.split(/[:,]/);
            let tsmilliseconds = parseInt(time[3]);
            let tsseconds = parseInt(time[2]);
            let tsminutes = parseInt(time[1]);
            let tshours = parseInt(time[0]);

            let delaymilliseconds = (this.delay % 1) * 1000;
            let delaynodecimal;
            let delayminutes;
            let delayhours;
            if (this.delay > 0) {
              delaynodecimal = Math.floor(this.delay);
              delayminutes = Math.floor(delaynodecimal / 60);
              delayhours = Math.floor(delaynodecimal / 3600);
            } else {
              delaynodecimal = Math.ceil(this.delay);
              delayminutes = Math.ceil(delaynodecimal / 60);
              delayhours = Math.ceil(delaynodecimal / 3600);
            }
            let delayseconds = delaynodecimal % 60;

            let newmilliseconds = Math.floor(tsmilliseconds + delaymilliseconds);
            let newseconds = tsseconds + delayseconds;
            let newminutes = tsminutes + delayminutes;
            let newhours = tshours + delayhours;

            if (newmilliseconds >= 1000) {
              newmilliseconds -= 1000;
              newseconds++;
            }

            if (newmilliseconds < 0) {
              newmilliseconds += 1000;
              newseconds--;
            }

            if (newseconds >= 60) {
              newseconds -= 60;
              newminutes++;
            }

            if (newseconds < 0) {
              newseconds += 60;
              newminutes--;
            }

            if (newminutes >= 60) {
              newminutes -= 60;
              newhours++;
            }

            if (newminutes < 0) {
              newminutes += 60;
              newhours--;
            }

            newtimestamps.push(`${newhours.toString().padStart(2, "0")}:${newminutes.toString().padStart(2, "0")}:${newseconds.toString().padStart(2, "0")},${newmilliseconds.toString().padStart(3, "0")}`);
          }
          newlines.push(`${newtimestamps[0]} --> ${newtimestamps[1]}`);
        } else {
          newlines.push(lines[i]);
        }
      }
      this.download(this.file.name.slice(0, -4) + " (Suki edition).srt", newlines.join("\n"));
    }).catch(err => {
      console.log(err);
      this.step3err = true;
    })
  }

  download(filename: string, text: string): void {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}
