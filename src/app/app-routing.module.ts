import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { SukiRTComponent } from './suki-rt/suki-rt.component';

const routes: Routes = [
  {
    path: 'sukirt',
    component: SukiRTComponent
  },
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
